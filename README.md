# plugin_linux_file_monitor

#### 介绍
使用 linux shell 脚本实现的 文件目录监控程序，可监控文件目录的变化并可记录日志和发出告警

使用场景：
1.	用于监控生产环境部署的可执行文件和配置文件，防止被意外修改，此时建议 XRK_WARN_METHO 配置   
    修改为 lock, 锁定文件目录的初始态。
2.	用于安全监控，对关键路径的文件目录监控防止挂马，此时建议 XRK_MONITOR_METHOD 配置文件 fmd5  
	XRK_WARN_OPR_INFO 设置为 all, 对所有变动进行告警


#### 安装教程

1.  在插件市场右键点击"安装插件"
2.  安装成功后，右键点击插件，下载部署配置文件和源码包
3.  在部署机上解压源码包，并将部署配置文件放入源码目录
4.  在配置文件：xrk_monitor_info_list.conf 中加入要监控的文件或者目录, 注意需要使用绝对路径
5.  [可选] 在配置文件：xrk_except_info_list.conf 中加入要排除监控的文件或者目录
6.  [可选] 打开配置文件：xrk_linux_file_monitor.conf, 根据配置说明和部署需要更改配置
7.	使用：./xrkmonitor_file_monitor.sh init 初始化监控状态
8.	执行: ./add_crontab.sh 定时运行监控脚本以便实时监控文件目录变化

#### 部署配置文件可选关键配置
部署配置文件：xrk_linux_file_monitor.conf, 以下配置比较关键可以根据需要自行修改

1.  XRK_WARN_METHO：监控模式, 可选值：lock - 锁定模式对变化持续产生监控日志和告警，dynamic -   
	监控状态随文件目录的变化而变化即对一次变动只侦测告警记录一次
2.  XRK_SCAN_SPEED_LEVEL: 扫描速度, 可选值0-5，值越小扫描越快但资源占用也越高, 如监控文件  
	目录太多系统资源占用较高时可以调整运行级别，建议不要监控超过1000个文件或者目录
3.  XRK_WARN_OPR_INFO: 监控告警配置, 可选值：all-全部告警,add-新增告警,del-删除告警,mod-修改   
	告警,no 不告警仅记录日志 
4.	XRK_MONITOR_METHOD: 文件变更的监控方法，可为 ftime/fmd5 分别表示时间戳和文件 md5   
	一般 time性能高，md5 更准确可靠

#### 监控系统上报情况示例
  
日志监控：
	![](http://xrkmonitor.com/monitor/images/file_monitor.png)

图表监控可登录演示系统查看：http://open.xrkmonitor.com/

