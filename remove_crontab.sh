#!/bin/bash
script_path=''
script_name=''
function in_script_path()
{
    echo "/" > _tmp
    fc=`expr substr "$0" 1 1`
    echo "$fc" > _tmp_2
    cmp _tmp _tmp_2 > /dev/null 2>&1
    if [ $? -eq 0 ];then
        cscript=$0
    else
        cscript=$(pwd)/$0
    fi
    rm -f _tmp _tmp_2
    script_path=`dirname $cscript`
    script_name=`basename $cscript`
    cd "$script_path"
}
in_script_path

crontab -l > _tmp 
grep "${script_path}; \./cron_linux_file_monitor\.sh check" _tmp > /dev/null 2>&1
if [ ! $? -eq 0 ]; then
        rm _tmp
        echo "already remove"
        exit 0
fi
 
nline=`grep -n "${script_path}; \./cron_linux_file_monitor\.sh check" _tmp |awk -F ":" '{print $1}'`
sed -i "${nline}d" _tmp
crontab _tmp 
if [ $? -eq 0 ];then
	rm _tmp
	echo "remove crontab ok"
	exit 0
fi

rm _tmp
echo "remove crontab failed"

