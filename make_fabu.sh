#!/bin/bash
PLUGIN=linux_file_monitor
TARGET=${PLUGIN}.tar.gz

if [ $# -ne 1 ];then
	echo "use ./make_fabu.sh open|cloud"
	exit 1
fi

COMM_FILE="start.sh stop.sh add_crontab.sh remove_crontab.sh restart.sh xrk_${PLUGIN}.sh xrk_monitor_info_list.conf xrkmonitor_report xrk_except_info_list.conf readme.txt auto_install.sh auto_uninstall.sh "
rm -f $TARGET > /dev/null 2>&1
if [ "$1" == "open" ]; then
	tar -czf ${TARGET} $COMM_FILE 
elif [ "$1" == "cloud" ];then
	tar -czf ${TARGET} $COMM_FILE xrk_${PLUGIN}.conf 
else
	echo "use ./make_fabu.sh open|cloud"
fi

if [ -d /srv/www/monitor/download/packet/linux ]; then
	echo "cp ${TARGET} /srv/www/monitor/download/packet/linux"
	cp ${TARGET} /srv/www/monitor/download/packet/linux
elif [ -d /home/monitor_cms/public/monitor/download/packet/linux ]; then
    echo "cp ${TARGET} /home/monitor_cms/public/monitor/download/packet/linux"
    cp ${TARGET} /home/monitor_cms/public/monitor/download/packet/linux
fi

