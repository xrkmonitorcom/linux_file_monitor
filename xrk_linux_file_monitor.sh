#!/bin/bash

#   xrkmonitor license 
#
#   Copyright (c) 2020 by rockdeng
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#
#   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
#   当前版本：v1.0
#   使用授权协议： apache license 2.0
#
#   开源版主页：http://open.xrkmonitor.com
#   云版本主页：http://xrkmonitor.com
#  
#
#   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
#   微信等多种方式，欢迎使用
#
#   监控插件 linux_file_monitor 功能:
#   		使用 linux shell 脚本实现的文件目录监控程序，可监控文件系统
#	文件目录的变化包括增删改，监控时对于目录是递归方式监控，即其下所
#	由子目录也将被监控。
#
#

if [ "${op_type}" == "" -o "${script_path}" == "" -o "${script_name}" == "" ]; then
    echo "invalid parameter ! "
    exit $LINENO
fi

report_exe=${xrkmonitor_bin}
xrkmonitor_config_file=${xrk_plugin_file_conf}

# 配置和变量设置检查 =====================================

# ^M 换行符输入方法先 ctrl+v, 然后 ctrl+m
tmp_info=`grep "^XRK_MONITOR_CONF_FILE" xrk_linux_file_monitor.conf |awk '{print $2}'|sed 's///g'`
file_info=$script_path/$tmp_info

tmp_info=`grep "^XRK_EXCEPT_MONITOR_INFO" xrk_linux_file_monitor.conf |awk '{print $2}'|sed 's///g'`
except_file_info=$script_path/$tmp_info

md5_info=`grep "^XRK_MD5_INFO_FILE" xrk_linux_file_monitor.conf |awk '{print $2}'|sed 's///g'`
time_info=`grep "^XRK_TIME_INFO_FILE" xrk_linux_file_monitor.conf |awk '{print $2}'|sed 's///g'`

warn_flag=`grep "^XRK_WARN_OPR_INFO" xrk_linux_file_monitor.conf |awk '{print $2}'|sed 's///g'`
if [ "$warn_flag" != "all" -a "$warn_flag" != "add" -a "$warn_flag" != "del" -a "$warn_flag" != "mod" -a "$warn_flag" != "no" ]; then
	$report_exe file "$xrkmonitor_config_file" "log" "error" "check warn info:$warn_flag failed, must be all|add|mod|del|no"
	exit $LINENO 
fi

scan_speed_level=`grep "^XRK_SCAN_SPEED_LEVEL" xrk_linux_file_monitor.conf |awk '{print $2}'|sed 's///g'`
if [ "$scan_speed_level" -lt 0 -o "$scan_speed_level" -gt 5 ]; then
	$report_exe file "$xrkmonitor_config_file" "log" "error" "check run level:$scan_speed_level failed, must be 0-5"
	exit $LINENO 
fi
scan_max=20
scan_limit=20
[ $scan_speed_level -eq 0 ] && scan_limit=$scan_max
[ $scan_speed_level -eq 1 ] && scan_limit=18
[ $scan_speed_level -eq 2 ] && scan_limit=12
[ $scan_speed_level -eq 3 ] && scan_limit=9
[ $scan_speed_level -eq 4 ] && scan_limit=5
[ $scan_speed_level -eq 5 ] && scan_limit=3

op_warn_method=`grep "^XRK_WARN_METHOD" xrk_linux_file_monitor.conf|awk '{print $2}'|sed 's///g'`
if [ "$op_warn_method" != "dynamic" -a "$op_warn_method" != "lock" ]; then
	$report_exe file "$xrkmonitor_config_file" "log" "error" "check monitor warn method:$op_method failed, must be dynamic or lock"
	exit $LINENO 
fi

auto_backup_max=`grep "^XRK_AUTO_BACKUP_COUNT" xrk_linux_file_monitor.conf|awk '{print $2}'|sed 's///g'`
if [ "$auto_backup_max" -lt 5 -o "$auto_backup_max" -gt 40 ]; then
	$report_exe file "$xrkmonitor_config_file" "log" "error" "invalid auto backup config:$auto_backup_max, must be 5-40"
	exit $LINENO 
fi

op_method=`grep "^XRK_MONITOR_METHOD" xrk_linux_file_monitor.conf|awk '{print $2}'|sed 's///g'`
if [ "$op_method" != "fmd5" -a "$op_method" != "ftime" ]; then
	$report_exe file "$xrkmonitor_config_file" "log" "error" "check monitor method:$op_method failed, must be ftime or fmd5"
	exit $LINENO
fi
if [ ! -f "$file_info" ]; then
	$report_exe file "$xrkmonitor_config_file" "log" "warn" "not find monitor config file: $file_info"
	exit $LINENO
fi

CURDATE=`date '+%Y%m%d-%H%M%S'`
function auto_backup()
{
	str_script_path=${script_path//\//.}
	if [ ! -d "$cache_prefix" ]; then
		mkdir -p "$cache_prefix" > /dev/null
		return;
	fi

	cd "$script_path"
	mv xrk_cache_info xrk_$CURDATE.cache_info
	COUNT=`find . -name "*-*cache_info" |wc -l`
	if [ $COUNT -gt $auto_backup_max ]; then
		find . -name "*-*cache_info"|sort|awk '{if(NR==1) print $1}' |xargs rm -fr {} \;
	fi
}

cache_prefix=$script_path/xrk_cache_info
if [ "$op_type" == "init" ]; then
	auto_backup
fi

#
# 从这里开始跑监控逻辑了 ============================
#

if [ $op_type != "init" ]; then
	$report_exe file "$xrkmonitor_config_file" "log" "info" "start check linux file monitor level:$scan_speed_level, limit:$scan_limit "
	if [ ! -f "$except_file_info" ];then
		$report_exe file "$xrkmonitor_config_file" "log" "error" "not find file:$except_file_info" 
		exit $LINENO
	fi
fi

if [ $op_type == "init" ]; then
# 去除首尾空白字符
    sed -i 's/^\s*//g' $except_file_info
    sed -i 's/\s*$//g' $except_file_info
    sed -i 's/^\s*//g' $file_info
    sed -i 's/\s*$//g' $file_info

	$report_exe file "$xrkmonitor_config_file" "log" "info" "start init linux file monitor "
fi
	

function xrkmonitor_warn()
{
	if [ "$2" -lt 1 ]; then
		return;
	fi
	if [ "$warn_flag" == "all" -o "$warn_flag" == "$1" ]; then
		$report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_DIR_FILE_UPDATE" "$2"
	fi
}

# 是否匹配排除文件或目录，返回 0表示匹配，返回1 表示不匹配
function is_except_dir_file()
{
    while read mpat
    do
        if [ "$mpat" == "" ]; then
            continue;
        fi
        spre=`expr substr "$mfile" 1 1`
        if [ "$spre" == "#" ]; then
            continue;
        fi

        echo "$1" |grep -e "$mpat" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            echo 0;
            return;
        fi
    done < "$except_file_info" 
    echo 1
}

function xrkmonitor_deal_dir()
{
	local same_c=0
	local mod_c=0
	local add_c=0
	local add_dir_c=0
	local ret_c=''
	local notsupport_c=0

	if [ "$op_type" == "init" ]; then
        ch=$(is_except_dir_file "$1")
		if [ $ch -eq 0 ]; then
			$report_exe file "$xrkmonitor_config_file" "log" "info" "init - except monitor dir: $1" > /dev/null 2>&1
			echo "$mod_c $add_c $same_c $add_dir_c $notsupport_c"
			return;
		fi

		local pdir=`dirname "$1"`
		[ ! -d "$cache_prefix/$pdir" ] && (mkdir -p "$cache_prefix/$pdir" > /dev/null 2>&1)
		cd "$cache_prefix/$pdir"
		[ ! -f "$md5_info" ] && (touch "$md5_info")
		[ ! -f "$time_info" ] && (touch "$time_info")
	
		local dtime=`stat -c %Y $1`
		echo "$dtime $1" >> "$time_info"
		same_c=1
		$report_exe file "$xrkmonitor_config_file" "log" "debug" "add monitor dir: $1 time:$dtime" > /dev/null 2>&1
	elif [ "$op_type" == "check" ]; then
        ch=$(is_except_dir_file "$1")
		if [ $ch -eq 0 ]; then
			$report_exe file "$xrkmonitor_config_file" "log" "info" "check - except monitor dir: $1" > /dev/null 2>&1
			echo "$mod_c $add_c $same_c $add_dir_c $notsupport_c"
			return
		fi

		local pdir=`dirname "$1"`
		local dfile=`basename "$1"`
		grep "^.\{10\} $1$" "$cache_prefix/$pdir/$time_info"  > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			$report_exe file "$xrkmonitor_config_file" "log" "warn" "dir: $1, is new add" > /dev/null 2>&1
			$report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_ADD_COUNT" "1"
			add_dir_c=1
			[ "$op_warn_method" == "dynamic" ] && (add_update_monitor "$pdir" "$dfile" "monitor_change_file" "add")
		else
			same_c=1
		fi
	fi

# 用于统计当前目录更新的文件数, 以便目录扫描完时上报数据到监控系统
	local tmp_c=0
	local f_mod_c=0
	local f_add_c=0
	local f_same_c=0
	local f_notsupport_c=0
	local scan_count=0

	cd "$1"
	local dfilelist=`ls -1`

	IFS_OLD="$IFS"
	IFS=$'\n'
	for fitem in $dfilelist
	do
		ret_c=$(xrkmonitor_deal_item "$1/$fitem")
		tmp_c=`echo $ret_c|awk '{print $1}'`
		mod_c=`expr $mod_c + $tmp_c`
		[ -f "$1/$fitem" -a $tmp_c -gt 0 ] && f_mod_c=`expr $f_mod_c + 1`

		tmp_c=`echo $ret_c|awk '{print $2}'`
		add_c=`expr $add_c + $tmp_c`
		[ -f "$1/$fitem" -a $tmp_c -gt 0 ] && f_add_c=`expr $f_add_c + 1`

		tmp_c=`echo $ret_c|awk '{print $3}'`
		same_c=`expr $same_c + $tmp_c`
		[ -f "$1/$fitem" -a $tmp_c -gt 0 ] && f_same_c=`expr $f_same_c + 1`

		tmp_c=`echo $ret_c|awk '{print $4}'`
		add_dir_c=`expr $add_dir_c + $tmp_c`

		tmp_c=`echo $ret_c|awk '{print $5}'`
		f_notsupport_c=`expr $notsupport_c + $tmp_c`
		[ -f "$1/$fitem" -a $tmp_c -gt 0 ] && f_notsupport_c=`expr $f_notsupport_c + 1`

		scan_count=`expr $scan_count + 1`
		if [ $scan_count -ge $scan_limit ]; then
			[ $f_add_c -gt 0 ] && $report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_ADD_COUNT" "$f_add_c"
			[ $f_mod_c -gt 0 ] && $report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_MOD_COUNT" "$f_mod_c"
			[ $f_same_c -gt 0 ] && $report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_SAME_COUNT" "$f_same_c"
			xrkmonitor_warn "add" "$f_add_c"
			xrkmonitor_warn "mod" "$f_mod_c"
			f_add_c=0
			f_mod_c=0
			f_same_c=0
			scan_count=0
			local sdelay=`expr $RANDOM % 2000000`
			[ $sdelay -lt 500000 ] && sdelay=500000
			usleep $sdelay 
		fi
	done
	IFS="$IFS_OLD"

	if [ $op_type == "init" ]; then
		$report_exe file "$xrkmonitor_config_file" "log" "info" "dir: $1, monitor count:$same_c"  >/dev/null 2>&1
	elif [ $add_c -gt 0 -o $add_dir_c -gt 0 -o $mod_c -gt 0 ]; then
		$report_exe file "$xrkmonitor_config_file" "log" "info" "dir: $1, monitor result - add file:$add_c, add dir:$add_dir_c, mod file:$mod_c, same:$same_c, not support:$notsupport_c" > /dev/null 2>&1
		[ $f_add_c -gt 0 ] && $report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_ADD_COUNT" "$f_add_c"
		[ $f_mod_c -gt 0 ] && $report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_MOD_COUNT" "$f_mod_c"
		[ $f_same_c -gt 0 ] && $report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_SAME_COUNT" "$f_same_c"
		local ff_tmp_change=`expr $f_add_c + $f_mod_c`
		[ $ff_tmp_change -gt 0 ] && $report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_DIR_FILE_UPDATE" "$ff_tmp_change"
	fi
	echo "$mod_c $add_c $same_c $add_dir_c $notsupport_c"
}

function add_update_monitor()
{
	[ ! -d "$cache_prefix/$1" ] && (mkdir -p "$cache_prefix/$1")
	echo "$4 $1/$2" >> "$cache_prefix/$1/$3"
}

function update_monitor_file()
{
	[ ! -d "$cache_prefix/$1" ] && (mkdir -p "$cache_prefix/$1")
	if [ "$op_method" == "fmd5" ]; then
		local ndel_tm=`grep -n "^.\{32\} $1/$2$" "$cache_prefix/$1/$3"|awk -F ":" '{print $1}'`
		if [ "$ndel_tm" != "" ]; then
			sed -i "${ndel_tm}d" "$cache_prefix/$1/$3" 
			local fmd5=`md5sum $1/$2|awk '{print $1}'`
			echo "$fmd5 $1/$2" >> "$cache_prefix/$1/$3"
			$report_exe file "$xrkmonitor_config_file" "log" "debug" "update monitor file: $1/$2 md5:$fmd5" > /dev/null 2>&1
		fi
	fi

	local ndel_tm=`grep -n "^.\{10\} $1/$2$" "$cache_prefix/$1/$4"|awk -F ":" '{print $1}'`
	if [ "$ndel_tm" != "" ]; then
		sed -i "${ndel_tm}d" "$cache_prefix/$1/$4" 
		local ftime=`stat -c %Y $1/$2`
		echo "$ftime $1/$2" >> "$cache_prefix/$1/$4"
		$report_exe file "$xrkmonitor_config_file" "log" "debug" "update monitor file: $1/$2 time:$ftime" > /dev/null 2>&1
	else
		$report_exe file "$xrkmonitor_config_file" "log" "error" "update monitor file: $1/$2 failed" > /dev/null 2>&1
	fi
}

function add_monitor_file()
{
	[ ! -d "$cache_prefix/$1" ] && (mkdir -p "$cache_prefix/$1")
	if [ "$op_method" == "fmd5" ]; then
		local fmd5=`md5sum $1/$2|awk '{print $1}'`
		echo "$fmd5 $1/$2" >> "$cache_prefix/$1/$3"
		$report_exe file "$xrkmonitor_config_file" "log" "debug" "add monitor file: $1/$2 md5:$fmd5" > /dev/null 2>&1
	fi

	local ftime=`stat -c %Y "$1/$2"`
	echo "$ftime $1/$2" >> "$cache_prefix/$1/$4"
	$report_exe file "$xrkmonitor_config_file" "log" "debug" "add monitor file: $1/$2 time:$ftime" > /dev/null 2>&1
}

function xrkmonitor_deal_file()
{
	if [ $op_type == "init" ]; then
        ch=$(is_except_dir_file "$1/$2")
		if [ $ch -eq 0 ]; then
			$report_exe file "$xrkmonitor_config_file" "log" "debug" "init - except monitor file: $1/$2" > /dev/null 2>&1
			echo "except"
			return;
		fi

		add_monitor_file "$1" "$2" "$md5_info" "$time_info"
		echo "init"
	else
        ch=$(is_except_dir_file "$1/$2")
		if [ $ch -eq 0 ]; then
			$report_exe file "$xrkmonitor_config_file" "log" "debug" "check - except monitor file: $1/$2" > /dev/null 2>&1
			echo "except"
			return;
		fi

		if [ "$op_method" == "fmd5" ]; then
			local last_ftmp=`grep "^.\{32\} $1/$2$" "$cache_prefix/$1/$md5_info"`
			if [ "$last_ftmp" == "" ]; then
				$report_exe file "$xrkmonitor_config_file" "log" "warn" "file: $1/$2 is new add" > /dev/null 2>&1
				echo "add"
				return
			fi

			local fmd5=`md5sum $1/$2|awk '{print $1}'`
			local last_fmd5=`echo "$last_ftmp" |awk '{print $1; }'`
			if [ "$fmd5" != "$last_fmd5" ]; then
				$report_exe file "$xrkmonitor_config_file" "log" "warn" "file: $1/$2 changed, md5 check: $fmd5 != $last_fmd5" > /dev/null 2>&1
				echo "mod"
			else
				$report_exe file "$xrkmonitor_config_file" "log" "debug" "file: $1/$2 md5:$fmd5 check ok" > /dev/null 2>&1
				echo "same"
			fi
		else 
			local last_ftmp=`grep "^.\{10\} $1/$2$" "$cache_prefix/$1/$time_info"`
			if [ "$last_ftmp" == "" ]; then
				$report_exe file "$xrkmonitor_config_file" "log" "warn" "file: $1/$2 is new add" > /dev/null 2>&1
				echo "add"
				return
			fi

			local ftime=`stat -c %Y $1/$2`
			local last_ftime=`echo "$last_ftmp"|awk '{print $1; }'`
			if [ "$ftime" != "$last_ftime" ]; then
				$report_exe file "$xrkmonitor_config_file" "log" "warn" "file: $1/$2 changed, time check: $ftime != $last_ftime" > /dev/null 2>&1
				echo "mod"
			else
				$report_exe file "$xrkmonitor_config_file" "log" "debug" "file: $1/$2 time:$ftime check ok" > /dev/null 2>&1
				echo "same"
			fi
		fi
	fi
}

function xrkmonitor_deal_item()
{
	local same_c=0
	local mod_c=0
	local add_c=0
	local add_dir_c=0
	local ret_c=''
	local tmp_c=0
	local notsupport_c=0

	if [ -d "$1" ]; then
		ret_c=$(xrkmonitor_deal_dir "$1")
		tmp_c=`echo $ret_c|awk '{print $1}'`
		mod_c=`expr $mod_c + $tmp_c`

		tmp_c=`echo $ret_c|awk '{print $2}'`
		add_c=`expr $add_c + $tmp_c`

		tmp_c=`echo $ret_c|awk '{print $3}'`
		same_c=`expr $same_c + $tmp_c`

		tmp_c=`echo $ret_c|awk '{print $4}'`
		add_dir_c=`expr $add_dir_c + $tmp_c` 

		tmp_c=`echo $ret_c|awk '{print $5}'`
		notsupport_c=`expr $notsupport_c + $tmp_c` 
	elif [ -f "$1" ]; then
		local fdir=`dirname "$1"`
		local ffile=`basename "$1"`
		local ret_c=$(xrkmonitor_deal_file "$fdir" "$ffile")
		if [ "$ret_c" == "add" ]; then
			add_c=1
			[ "$op_warn_method" == "dynamic" ] && (add_update_monitor "$fdir" "$ffile" "monitor_change_file" "$ret_c")
		elif [ "$ret_c" == "mod" ]; then
			[ "$op_warn_method" == "dynamic" ] && (add_update_monitor "$fdir" "$ffile" "monitor_change_file" "$ret_c")
			mod_c=1
		elif [ "$ret_c" == "init" -o "$ret_c" == "same" ]; then
			same_c=1
		fi
	elif [ -e "$1" ]; then
		$report_exe file "$xrkmonitor_config_file" "log" "info" "not support file: $1" > /dev/null 2>&1
		notsupport_c=1
	else
		$report_exe file "$xrkmonitor_config_file" "log" "error" "file: $1 not exist" > /dev/null 2>&1
	fi
	echo "$mod_c $add_c $same_c $add_dir_c $notsupport_c"
}


t_same_c=0
t_del_c=0
t_mod_c=0
t_add_c=0
t_add_dir_c=0
t_ret_c=''
t_tmp_c=0
t_notsupport_c=0

while read mfile
do
    sdir=`expr substr "$mfile" 1 1`
    if [ "$sdir" == "#" ]; then
        # 注释
        continue;
    fi 

    if [ "$sdir" == "." ]; then
        #相对路径转绝对路径
        mfile="$script_path/$mfile"
    fi

	t_ret_c=$(xrkmonitor_deal_item "$mfile")
	t_tmp_c=`echo $t_ret_c|awk '{print $1}'`
	t_mod_c=`expr $t_mod_c + $t_tmp_c`

	t_tmp_c=`echo $t_ret_c|awk '{print $2}'`
	t_add_c=`expr $t_add_c + $t_tmp_c`

	t_tmp_c=`echo $t_ret_c|awk '{print $3}'`
	t_same_c=`expr $t_same_c + $t_tmp_c`

	t_tmp_c=`echo $t_ret_c|awk '{print $4}'`
	t_add_dir_c=`expr $t_add_dir_c + $t_tmp_c`

	t_tmp_c=`echo $t_ret_c|awk '{print $5}'`
	t_notsupport_c=`expr $t_notsupport_c + $t_tmp_c`
done < $file_info

function xrkmonitor_deal_del()
{
	$report_exe file "$xrkmonitor_config_file" "log" "warn" "$2: is delete" > /dev/null 2>&1
	if [ "$op_warn_method" == "dynamic" ]; then
		ndel_info=`grep -n "^.\{10\} $2$" "$1/$time_info"`
		if [ "$ndel_info" != "" ]; then
			ndel=`echo "$ndel_info"|awk -F ":" '{print $1}'`
			sed -i "${ndel}d" "$1/$time_info"
		fi
		if [ "$op_method" == "fmd5" ]; then
			ndel_info=`grep -n "^.\{32\} $2$" "$1/$md5_info"`
			if [ "$ndel_info" != "" ]; then
				ndel=`echo "$ndel_info"|awk -F ":" '{print $1}'`
				sed -i "${ndel}d" "$1/$md5_info"
			fi
		fi
	fi	
}

function monitor_file_del()
{
	cd "$1"
	local t_del_c=0
	local dfilelist=`ls -1`
	local t_ret=0
	for fitem in $dfilelist
	do
		if [ ! -d "$1/$fitem" ]; then
			continue;
		fi

        ch=$(is_except_dir_file "$fitem")
		if [ $ch -eq 0 ]; then
			continue
		fi
		t_ret=$(monitor_file_del  "$1/$fitem")
		t_del_c=`expr $t_del_c + $t_ret`
	done 

	if [ -f "$1/${time_info}" ]; then
		while read cfile_info
		do
			mfile=${cfile_info:11}
			if [ ! -e "$mfile" ]; then
                ch=$(is_except_dir_file "$mfile")
	        	if [ $ch -eq 0 ]; then
					continue
				fi
				xrkmonitor_deal_del "$1" "$mfile" > /dev/null 2>&1
				t_del_c=`expr $t_del_c + 1`
			fi
		done < "$1/${time_info}"
	fi

	if [ $t_del_c -gt 0 ]; then
		$report_exe file "$xrkmonitor_config_file" "attr" "add" "XRK_DEL_COUNT" "$t_del_c" > /dev/null 2>&1
		xrkmonitor_warn "del" "$t_del_c" > /dev/null 2>&1
	fi
	echo "$t_del_c"
}

# 增量监控, 增加/修改文件/目录处理
function update_monitor_info()
{
	cd "$1"
	local dfilelist=`ls -1`
	for fitem in $dfilelist
	do
		if [ -d "$1/$fitem" ]; then
			update_monitor_info "$1/$fitem"
			continue;
		fi
	done

	if [ ! -f "$1/monitor_change_file" ]; then
		return;
	fi

	while read xrkfname 
	do
		local opr=`echo "$xrkfname"|awk '{print $1}'`
		local oprfile=`echo "$xrkfname"|awk '{print $2}'`
		local ffile=`basename "$oprfile"`
		local fdir=`dirname "$oprfile"`
		if [ -f "$oprfile" ]; then
			if [ "$opr" == "add" ]; then
				add_monitor_file "$fdir" "$ffile" "$md5_info" "$time_info"
			elif [ "$opr" == "mod" ]; then
				update_monitor_file "$fdir" "$ffile" "$md5_info" "$time_info"
			fi
		elif [ -d "$oprfile" ]; then
			add_monitor_file "$fdir" "$ffile" "$md5_info" "$time_info"
		fi
	done < "$1/monitor_change_file"
	rm -f "$1/monitor_change_file" > /dev/null 2>&1
}

if [ "$op_type" == "check" ]; then
	t_del_c=$(monitor_file_del "$cache_prefix")
	[ "$op_warn_method" == "dynamic" ] && update_monitor_info "$cache_prefix"
	$report_exe file "$xrkmonitor_config_file" "log" "info" " last check result: same($t_same_c), not support($t_notsupport_c), add file($t_add_c), add dir($t_add_dir_c), mod($t_mod_c), del($t_del_c) ----- "
else
	$report_exe file "$xrkmonitor_config_file" "log" "info" " init result: total($t_same_c), not support($t_notsupport_c) ----- "
fi

check_process_atexit
exit 0


