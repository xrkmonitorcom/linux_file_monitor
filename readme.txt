xrkmonitor_report 开源版上报工具
xrkmonitor_report_cloud 云版本上报工具

工具编译:
进入 linux_shell 目录，执行 make 命令生成根据环境可生成开源版或云版的 xrkmonitor_report 
执行make 前需要先初始化云版本或者开源版编译环境(初始化方法查看 makefile 文件说明)

使用方法：
1. 在开源版web控制台插件市场安装该插件
2. 下载部署配置文件： xrk_linux_file_monitor.conf
3. 在 xrk_monitor_info_list.conf 配置文件中添加上要监控的目录和文件，目录可以是绝对路径
    或者相对于脚本路径, 目录条目其下的所有文件和目录都会被监控
4. [ 可选 ] 根据需要在 xrk_except_info_list.conf 中添加要排除监控的文件或者目录
5. 控制台执行：./cron_linux_file_monitor.sh init &, 初始化监控信息
   如需调试可在： xrk_linux_file_monitor.conf 文件中的修改本地日志配置以便调试
6. 执行 ./add_crontab.sh 将监控脚本加入crontab 定时执行
7. 部署好后可在web控制台查看监控图表和日志


排除配置文件： xrk_except_info_list.conf 支持正则表达式


