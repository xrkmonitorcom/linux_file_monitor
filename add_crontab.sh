#!/bin/bash
script_path=''
script_name=''
function in_script_path()
{
    echo "/" > _tmp
    fc=`expr substr "$0" 1 1`
    echo "$fc" > _tmp_2
    cmp _tmp _tmp_2 > /dev/null 2>&1
    if [ $? -eq 0 ];then
        cscript=$0
    else
        cscript=$(pwd)/$0
    fi
    rm -f _tmp _tmp_2
    script_path=`dirname $cscript`
    script_name=`basename $cscript`
    cd "$script_path"
}
in_script_path

crontab -l > _add_xrk_plugin_check_tmp 
grep "${script_path}; \./cron_linux_file_monitor\.sh check" _add_xrk_plugin_check_tmp > /dev/null 2>&1
if [ $? -eq 0 ]; then
	rm _add_xrk_plugin_check_tmp 
	echo "already add"
	exit 0
fi

curpwd=`pwd`
echo "*/1 * * * * cd ${script_path}; ./cron_linux_file_monitor.sh check > /dev/null 2>&1" >> _add_xrk_plugin_check_tmp 

crontab _add_xrk_plugin_check_tmp
if [ $? -eq 0 ]; then
	rm _add_xrk_plugin_check_tmp
	echo "add crontab ok"
	exit 0
fi

echo "add to crontab failed"
rm _add_xrk_plugin_check_tmp 

